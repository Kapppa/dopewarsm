;########################################################################################
;#                                  Dopwarsm                                            #
;########################################################################################
;#                                                                                      #
;# It was back in 1984 that John E. Dell developed a game called Drugwars. The game was #
;# apparently inspired by Star Trader, a game where you'd travel the galaxy buying and  #
;# selling a defined set of commodities. Drugwars, of course, dealt with dealing drugs. #
;# As a teen I discovered Dopewars, an evolution of Drugwars, developed in 1985 by the  #
;# Happy Hacker Foundation. I must admit spending quite some idle time with it.         #
;# Since I love miniatures and couldn't find an ASM version of it, here is an attempt   #
;# at recreating that atmosphere, the smallest size I can, Dopewarsm.                   #
;# By the way, I'm really learning ASM only now, have mercy.                            #
;#                                                                                      #
;# https://en.wikipedia.org/wiki/Drug_Wars_(video_game)                                 #
;#                                                                                      #
;# Made by Ka in 2021                                                                   #
;#                                                                                      #
;########################################################################################


use16
org 0x0100

macro enum var, [stuff] {
      common
            local x
            x=0
            struc var \{. dd  ?\}
      forward
            x=x+1
            stuff=x
}

macro clrscr {
      mov ah, 0
      mov al, 3
      int 10h
}

macro print_menu_item x, y, xx, item_letter, item_text, item_value {
      mov dl, x
      mov dh, y
      call gotoxy
      mov al, item_letter
      call print_char
      mov al, 3Eh
      call print_char
      mov al, 20h
      call print_char
      print_item x+3, y, xx, item_text, item_value
}

macro print_item x, y, xx, item_text, item_value {
      mov [pos_x], x
      mov [pos_y], y
      mov [pos_xx], xx
      mov si, item_text
      mov [item_name], si
      mov eax, dword [item_value]
      mov [item_amount], eax
      call print_item_routine
}

macro print_city x, y, city_letter, city_name {
      mov dl, x
      mov dh, y
      call gotoxy
      mov al, city_letter
      call print_char
      mov al, 3Eh
      call print_char
      mov al, 20h
      call print_char
      mov si, city_name
      call print
}

;Here we go
_start:

mov ebp, esp; for correct debugging

;Let's start with a blank screen
clrscr
 mov dl, 0
 mov dh, 0
call gotoxy
mov si, msg_top
call print
mov dh, 23
mov dl, 0
loop_main_frame:
call gotoxy
mov si, msg_line
call print
dec dh
cmp dh, 1
jge loop_main_frame
mov dl, 0
mov dh, 23
call gotoxy
mov si, msg_bottom
call print

mov ecx, 7
display_intro:
mov dl, 7
mov dh, cl
call gotoxy
mov si, [intro_ptrs + (ecx-7)*2]
call print
inc ecx
cmp ecx, 14d
jl display_intro
mov ah, 0
int 0x16   ; wait for keypress

call srandsystime   ; Seed PRNG with system time, call once only

;Draw main screen
;This is just the beginning
mov dh, 22
.loop_top:
mov dl, 0
call gotoxy
mov si, msg_line
call print
dec dh
cmp dh, 0
jne .loop_top
mov dl, 0
mov dh, 0
call gotoxy
mov si, msg_top
call print
mov dl, 35
mov dh, 1
call gotoxy
mov si, msg_title
call print
new_day_check:
mov ah, [day_inc]
inc ah
cmp ah, 3
jge .new_day
jmp another_day
.new_day:
mov ah, [day]
inc ah
cmp ah, 31d
je end_game
mov [day], ah
mov [day_inc], 0
;Passive income
mov eax, [bank]
mov cx, 102
mul cx
add eax, 50
adc dx, 0
mov cx, 100
div cx
mov [bank], eax
;Passive Debt
mov eax, [debt]
mov cx, 105
mul cx
add eax, 50
adc dx, 0
mov cx, 100
div cx
mov [debt], eax
another_day:
mov ah, [day_inc]
inc ah
mov [day_inc], ah
mov dl, 61
mov dh, 1
call gotoxy
movsx ax, [day]
mov cx, 10
mov si, output_string
call itoa
call print
mov si, msg_date
call print
new_prices:
;Check current street prices
call calculate_prices
main_screen:
mov si, msg_screen_top
mov dl, 0
mov dh, 2
call gotoxy
call print
mov si, msg_screen_middle
mov dl, 0
mov dh, 3
call gotoxy
call print
mov dl, 7
mov dh, 3
call gotoxy
mov si, msg_stats
call print

;Print City
mov dl, 37
mov dh, 3
call gotoxy
call get_city
call print

;Print Trenchcoat
mov dl, 60
mov dh, 3
call gotoxy
mov si, msg_trenchcoat
call print
mov si, msg_screen_bottom
mov dl, 0
mov dh, 4
call gotoxy
call print

;Stuff in the middle
mov dh, 5
loop_center:
mov si, msg_screen_middle_line
mov dl, 0
call gotoxy
call print
inc dh
cmp dh, 16
jne loop_center

;This is the top of the "stats" but I called it "menu"
mov dl, 0
mov dh, 16
call gotoxy
mov si, msg_screen_top_menu
call print

;Here we print the Stats
;Print Cash
print_item 7, 6, 17, msg_cash, cash

;Print Bank
print_item 7, 8, 17, msg_bank, bank

;Print Debt
print_item 7, 10, 17, msg_debt, debt


;Print what we have in store

; cocaine_price dw 22000
; hashish_price dw 800
; heroin_price dw 9000
; ludes_price dw 10
; mda_price dw 2000
; opium_price dw 700
; peyote_price dw 600
; shrooms_price dw 800
; speed_price dw 200
; weed_price dw 300
; acid_price dw 1000
; pcp_price dw 1000

print_item 43, 6, 53, msg_cocaine, drugs_store
print_item 65, 6, 75, msg_hashish, drugs_store+2

print_item 43, 7, 53, msg_heroin, drugs_store+4
print_item 65, 7, 75, msg_ludes, drugs_store+6

print_item 43, 8, 53, msg_mda, drugs_store+8
print_item 65, 8, 75, msg_opium, drugs_store+10

print_item 43, 10, 53, msg_peyote, drugs_store+12
print_item 65, 10, 75, msg_shrooms, drugs_store+14

print_item 43, 11, 53, msg_speed, drugs_store+16
print_item 65, 11, 75, msg_weed, drugs_store+18

print_item 43, 12, 53, msg_acid, drugs_store+20
print_item 65, 12, 75, msg_pcp, drugs_store+22

;Business question
what_to_do:
mov dh, 21
.loop_center_again:
mov dl, 0
call gotoxy
mov si, msg_line
call print
dec dh
cmp dh, 17
jne .loop_center_again

mov dl, 3
mov dh, 17
call gotoxy
mov si, msg_hey_dude
call print

;Start the Drug letters
mov al, 40h
pusha

;First row of drugs
;     Cocaine
print_menu_item 5, 18, 18, 41h, msg_cocaine, drug_price

;     Hashish
print_menu_item 30, 18, 45, 42h, msg_hashish, drug_price+2

;     Heroin
print_menu_item 56, 18, 70, 43h, msg_heroin, drug_price+4

;Second row of drugs
;     Ludes
print_menu_item 5, 19, 18, 44h, msg_ludes, drug_price+6

;     MDA
print_menu_item 30, 19, 45, 45h,  msg_mda, drug_price+8

;     Opium
print_menu_item 56, 19, 70, 46h, msg_opium, drug_price+0Ah

;Third row of drugs
;     Peyote
print_menu_item 5, 20, 18, 47h, msg_peyote, drug_price+0Ch

;     Shrooms
print_menu_item 30, 20, 45, 48h, msg_shrooms, drug_price+0Eh

;     Speed
print_menu_item 56, 20, 70, 49h, msg_speed, drug_price+10h

;Fourth row of drugs
;     Weed
print_menu_item 5, 21, 18, 4Ah, msg_weed, drug_price+12h

;     Acid
print_menu_item 30, 21, 45, 4Bh, msg_acid, drug_price+14h

;     PCP
print_menu_item 56, 21, 70, 4Ch, msg_pcp, drug_price+16h
;End of drug display

;What do you want to do?
mov dl, 0
mov dh, 22
call gotoxy
mov si, msg_line
call print
mov dl, 3
mov dh, 22
call gotoxy
mov si, msg_what_to_do
call print

;Bottom stuff
mov dl, 0
mov dh, 23
call gotoxy
mov si, msg_bottom
call print

;Get into the input loop
mov si, buffer
call get_input

end_game:
clrscr
mov dl, 10
mov dh, 10
call gotoxy
mov si, msg_end_game
call print
mov dl, 10
mov dh, 11
call gotoxy
mov si, msg_you_earned
call print
mov eax, [cash]
sub eax, [debt]
mov cx, 10
mov si, output_string
call itoa
call print
;Exit
int 20h

get_input:
   xor cl, cl

 .loop:
   mov ah, 0
   int 0x16   ; wait for keypress

   ;Check what is typed
   ; x = exit game
   ; j = Jet, go some other city
   ; b = you want to buy a drug, then type the corresponding letter
   ; s = you wan to sell a drug, then type the corresponding letter

   ;Exit
   cmp al, 78h ; x = exit?
   je .exit

   ;Jet
   cmp al, 6ah ; j = Jet?
   je .jet

   ;Buy
   cmp al, 62h ; b = Buy?
   je .buy

   ;Sell
   cmp al, 73h ; s = Sell?
   je .sell

   ;Something else, let's go back to main screen
   jmp main_screen

.exit:
      clrscr
      int 20h

.jet:
      mov dl, 0
      mov dh, 21
      .loop_screen_jet:
      call gotoxy
      mov si, msg_line
      call print
      dec dh
      cmp dh, 17
      jne .loop_screen_jet

      ;Bronx
      print_city 7, 18, 41h, msg_bronx
      ;Ghetto
      print_city 30, 18, 42h, msg_ghetto
      ;Central
      print_city 52, 18, 43h, msg_central_park
      ;Coney Island
      print_city 7, 19, 44h, msg_coney_island
      ;Manhattan
      print_city 30, 19, 45h, msg_manhattan
      ;Brooklyn
      print_city 52, 19, 46h, msg_brooklyn

      ;Where to Jet?
      mov dl, 2
      mov dh, 22
      call gotoxy
      mov si, msg_line
      call print
      mov dl, 3
      mov dh, 22
      call gotoxy
      mov si, msg_where_to_jet
      call print
      ; wait for keypress
      mov ah, 0
      int 0x16   
      ;Check if input is valid
      cmp al, 60h
      jle main_screen
      cmp al, 67h
      jge main_screen
      sub al, 60h
      cmp al, [city]
      je main_screen
      mov [city], al
      cmp al, BRONX
      je .bank_or_shark

      jmp new_day_check

.bank_or_shark:
      mov dl, 0
      mov dh, 22
      call gotoxy
      mov si, msg_line
      call print
      mov dl, 3
      mov dh, 22
      call gotoxy
      mov si, msg_bank_or_shark
      call print

      mov ah, 0
      int 0x16   ; wait for keypress

      ;Check what is typed
      cmp al, 62h
      je .bank
      cmp al, 6ch
      je .shark            
      jmp another_day

.bank:
      mov dl, 0
      mov dh, 22
      call gotoxy
      mov si, msg_line
      call print
      mov dl, 3
      mov dh, 22
      call gotoxy
      mov si, msg_bank_deposit_or_witdraw
      call print

      mov ah, 0
      int 0x16   ; wait for keypress

      cmp al, 64h
      je .bank_deposit
      cmp al, 77h
      je .bank_withdraw

      jmp another_day

.bank_deposit:
      mov dl, 0
      mov dh, 22
      call gotoxy
      mov si, msg_line
      call print
      mov dl, 3
      mov dh, 22
      call gotoxy
      mov si, msg_bank_deposit
      call print
      mov  di, buffer
      call keyboard_input
      mov  si, buffer
      call atoi
      cmp [cash], eax
      jl .bank_or_shark
      add [bank], eax
      sub [cash], eax
      
      ;Print Cash
      print_item 7, 6, 17, msg_cash, cash
      
      ;Print Bank
      print_item 7, 12, 17, msg_bank, bank
      jmp .bank_or_shark    

.bank_withdraw:
      mov dl, 0
      mov dh, 22
      call gotoxy
      mov si, msg_line
      call print
      mov dl, 3
      mov dh, 22
      call gotoxy
      mov si, msg_bank_withdraw
      call print
      mov  di, buffer
      call keyboard_input
      mov  si, buffer
      call atoi
      cmp [bank], eax
      jl .bank_or_shark
      sub [bank], eax
      add [cash], eax
      ;Print Cash
      print_item 7, 6, 17, msg_cash, cash
      ;Print Bank
      print_item 7, 12, 17, msg_bank, bank
      jmp .bank_or_shark    

.shark:
      mov dl, 0
      mov dh, 22
      call gotoxy
      mov si, msg_line
      call print
      mov dl, 3
      mov dh, 22
      call gotoxy
      mov si, msg_shark
      call print
      mov  di, buffer
      call keyboard_input
      mov  si, buffer
      call atoi
      cmp [debt], eax
      jl .bank_or_shark
      sub [debt], eax
      sub [cash], eax
      ;Print Cash
      print_item 7, 6, 17, msg_cash, cash
      ;Print Debt
      print_item 7, 14, 17, msg_debt, debt
      jmp .bank_or_shark    

.buy:
      mov dl, 0
      mov dh, 22
      call gotoxy      
      mov si, msg_line
      call print
      mov dl, 3
      mov dh, 22
      call gotoxy
      mov si, msg_what_to_buy
      call print
      xor cl, cl

      mov ah, 0
      int 0x16   ; wait for keypress

      ;Check what is typed
      cmp al, 60h
      jle main_screen
      cmp al, 73h
      jge main_screen

      call .buy_drug

      jmp main_screen

 .buy_done:
   mov al, 0    ; null terminator
   stosb

   mov ah, 0x0E
   mov al, 0x0D
   int 0x10
   jmp main_screen

.buy_how_many:
    mov dl, 0
    mov dh, 22
    call gotoxy
    mov si, msg_line
    call print
    ;Display how many you can buy
    mov edx, 0 
    mov eax, [cash]
    movsx ecx, [chosen_drug]
    movsx ebx, [drug_price + ecx*2]
    div ebx
    mov [units], ax
    print_item 43, 22, 55, msg_how_many_you_can_afford, units
    mov dl, 3
    mov dh, 22
    call gotoxy
    mov si, msg_how_many_units
    call print        
    cld
    mov  di, buffer
    call keyboard_input
    int3
    ret

.buy_drug:
    sub al, 61h
    mov [chosen_drug], al
    call .buy_how_many
    mov  si, buffer
    call atoi
    mov [chosen_units], ax    
    movsx ecx, [chosen_drug]
    ;Get chosen drug price    
    mov ax, [drug_price + ecx*2]
    mov bx, [chosen_units]
    mul bx
    mov ebx, [cash]    
    ;Do I have enough Cash?
    cmp ax, bx
    jg main_screen
    ;Process the Transaction
    sub bx, ax
    mov [cash], ebx
    mov ax, [chosen_units]
    movsx ecx, [chosen_drug]
    mov bx, [drugs_store + ecx*2]
    add ax, bx
    mov [drugs_store + ecx*2], ax
    ;Don't forget to leave the Customer receipt!
    ret

.sell:
      mov dl, 0
      mov dh, 22
      call gotoxy
      mov si, msg_line
      call print
      mov dl, 3
      mov dh, 22
      call gotoxy
      mov si, msg_what_to_sell
      call print
      xor cl, cl

 .sell_input:
   mov ah, 0
   int 0x16   ; wait for keypress
   ;Check what is typed
   cmp al, 60h
   jle main_screen
   cmp al, 73h
   jge main_screen
   call .sell_drug
   jmp main_screen

 .sell_done:
   mov al, 0    ; null terminator
   stosb
   mov ah, 0x0E
   mov al, 0x0D
   int 0x10
   jmp main_screen

.sell_how_many:
      mov dl, 0
      mov dh, 22
      call gotoxy
    mov si, msg_line
    call print
    ;Display how many you can sell
    mov edx, 0 
    movsx ecx, [chosen_drug]
    movsx eax, [drugs_store + ecx*2]
    mov [units], ax
    print_item 43, 22, 55, msg_how_many_you_have, units
      mov dl, 3
      mov dh, 22
      call gotoxy
    mov si, msg_how_many_units
    call print
    ret

.sell_drug:
    sub al, 61h
   ;Do I have this?
    movsx ecx, [chosen_drug]
    mov ax, [drugs_store + ecx*2]
    cmp ax, 0
    je main_screen    
    ;Ask how many to sell
    mov [chosen_drug], al
    call .sell_how_many
    cld
    mov  di, buffer
    call keyboard_input
    call atoi
    mov [chosen_units], ax    
    ;Do I have enough Units?
    movsx ecx, [chosen_drug]
    mov ax, [drugs_store + ecx*2]
    mov bx, [chosen_units]
    cmp ax, bx
    jl main_screen
    ;Process the Transaction
    movsx ecx, [chosen_drug]
    mov eax, dword [drug_price + ecx*2]
    mov ebx, dword [chosen_units]
    mul ebx
    mov ebx, [cash]
    add eax, ebx
    mov [cash], eax    
    mov bx, [chosen_units]
    movsx ecx, [chosen_drug]
    mov ax, [drugs_store + ecx*2]
    sub ax, bx
    mov [drugs_store + ecx*2], ax
    ;Don't forget to leave the Customer receipt!
    ret

keyboard_input:
    mov  ah, 00h
    int  16h
    cmp  al, 8
    je keyboard_remove_backspace
    mov ah, 0x0E
    int 0x10      ; print out character
    stosb
    inc cl
    cmp  al, 13d
    jne  keyboard_input
    mov al, 0   ; null terminator
    ret

keyboard_remove_backspace:
      cmp cl, 0 ; beginning of string?
      je keyboard_input ; yes, ignore the key
      dec di
      mov byte [di], 0  ; delete character
      dec cl            ; decrement counter as well
      mov ah, 0x0E
      mov al, 0x08
      int 10h           ; backspace on the screen
      mov al, ' '
      int 10h           ; blank character out
      mov al, 0x08
      int 10h           ; backspace again
      jmp keyboard_input

get_city:
      movsx ecx, [city]
      dec ecx
      mov si, [city_ptrs + ecx * 2]
      ret

get_price_factor:
      call rand           ; Get a random number in AX
      mov bx, 3
      call rand2num  ; Convert AX to num between 1 and 3
      mov [price_factor1], ax
      call rand           ; Get a random number in AX
      mov bx, 7
      call rand2num  ; Convert AX to num between 1 and 7
      mov [price_factor2], ax
      mov ax, [price_factor1]
      mov dx, [price_factor2]
      mul dx
      mov [price_factor], ax
      ret

calculate_prices:            
      mov ecx, 11
      .loop_prices:
      call get_price_factor      
      mov ax, [drug_price_base+ecx*2]
      mov dx, [price_factor]
      add ax, dx
      mov [drug_price+ecx*2], ax
      dec ecx
      cmp ecx, 0
      jge .loop_prices
      ret

; start of procs

;title  ITOA --- Convert 16-bit integer to ASCII
;       page    55,132
; ITOA.ASM --- Convert 16-bit integer to ASCII string
;
; Copyright (C) 1989 Ray Duncan
;
; Call with: AX    = 16-bit integer
;                DS:SI = buffer to receive string,
;                       must be at least 6 bytes long
;                CX    = radix
;
; Returns:       DS:SI = address of converted string
;                AX    = length of string
;
; Destroy:      Nothing
;
; Since test for value = zero is made after a digit
; has been stored, the resulting string will always
; contain at least one significant digit.
itoa:
        push dx
        add     si,6                    ; advance to end of buffer
        push    si                      ; and save that address.
        or      ax,ax                   ; test sign of 16-bit value,
        pushf                           ; and save sign on stack.
        jns     itoa1                   ; jump if value was positive.
        neg     ax                      ; find absolute value.

itoa1:  mov     dx,0                    ; divide value by radix to extract
        div     cx                      ; next digit for forming string
        add     dl,'0'                  ; convert remainder to ASCII digit
        cmp     dl,'9'                  ; in case converting to hex ASCII,
        jle     itoa2                   ; jump if in range 0-9,
        add     dl,'A'-'9'-1            ; correct digit if in range A-F

itoa2:  dec     si                      ; back up through buffer
        mov     [si],dl                 ; store this character into string
        or      ax,ax
        jnz     itoa1                   ; no, convert another digit

        popf                            ; was original value negative?
        jns     itoa3                   ; no, jump
        dec     si                      ; yes, store sign into output
        mov     byte [si],'-'

itoa3:  pop     ax                      ; calculate length of string
        sub     ax,si
        pop     dx
        ret                             ; back to caller


; ATOI.ASM --- Convert ASCII string to 16-bit decimal integer.
;
; Copyright (C) 1989 Ray Duncan
;
; Call with:    DS:SI = address of string,
;               where 'string' is in the form
;                  [whitespace][sign][digits]
;
; Returns:      AX    = result
;               DS:SI = address+1 of terminator
;
; Destroys:     Nothing
;
; Like the C runtime library function 'atoi', this routine gives no
; warning of overflow, and terminates on the first invalid character.
atoi:
        push    bx                      ; save registers
        push    cx
        push    dx

        xor     bx,bx                   ; initialize forming answer
        xor     cx,cx                   ; initialize sign flag

atoi1:  lodsb                           ; scan off whitespace
        cmp     al,20h                  ; ignore leading blanks
        je      atoi1
        cmp     al,9h                   ; ignore leading tabs
        je      atoi1

        cmp     al,'+'                  ; if + sign, proceed
        je      atoi2
        cmp     al,'-'                  ; is it - sign?
        jne     atoi3                   ; no, test if numeric
        dec     cx                      ; was - sign, set flag
                                        ; for negative result

atoi2:  lodsb                           ; get next character

atoi3:  cmp     al,'0'                  ; is character valid?
        jb      atoi4                   ; jump if not '0' to '9'
        cmp     al,'9'
        ja      atoi4                   ; jump if not '0' to '9'

        and     ax,0fh                  ; isolate lower four bits

        xchg    bx,ax                   ; previous answer x 10
        mov     dx,10
        mul     dx

        add     bx,ax                   ; add this digit

        jmp     atoi2                   ; convert next digit

atoi4:  mov     ax,bx                   ; put result into AX
        jcxz    atoi5                   ; jump if sign flag clear
        neg     ax                      ; make result negative

atoi5:  pop     dx                      ; restore registers
        pop     cx
        pop     bx
        ret                             ; back to caller

print:
      .nextChar:
        lodsb               ; load string byte and stores it in AL

        cmp al, 0
        je .done            ; If char is zero, end of string detected!

        mov ah, 0x0E        ; single character BIOS print service
        mov bh, 0x00        ; Page num 0
        mov bl, 0x07        ; Text Color attributes
        int 0x10            ; Call BIOS video interrupt
        jmp .nextChar       ; follow with next char
    .done:
        RET

print_char:
      mov ah, 0x0E        ; single character BIOS print service
      mov bh, 0x00        ; Page num 0
      mov bl, 0x07        ; Text Color attributes
      int 0x10
      ret

print_item_routine:
      mov dl, [pos_x]
      mov dh, [pos_y]
      call gotoxy
      mov di, item_name
      call print
      mov dl, [pos_xx]
      mov dh, [pos_y]
      call gotoxy
      mov ax, word [item_amount]
      mov cx, 10
      mov si, output_string
      call itoa
      call print
      ret

gotoxy:
      mov ah, 2
      mov bh, 0
;      mov dl, x
;      mov dh, y
      int 10h
      ret


; Return number between 1 and whatever
;
; Inputs:   AX = value to convert
; Return:   (AX) value between 1 and 10

rand2num:
    push bx
    push dx
    xor dx,dx           ; Compute randval(DX) mod X to get num
    div bx
    inc dx              ; DX = modulo from division
                        ;     Add 1 to give us # between 1 and X (not 0 to X-1)
    mov ax,dx
    pop bx
    pop dx
    ret

; Set LCG PRNG seed to system timer ticks
;
; Inputs:   AX = seed
; Modifies: AX
; Return:   nothing

srandsystime:
    push cx
    push dx
    xor ax, ax          ; Int 1Ah/AH=0 to get system timer in CX:DX
    int 1ah
    mov [seed], dx      ; seed = 16-bit value from DX
    pop dx
    pop cx
    ret

; Updates seed for next iteration
;     seed = (multiplier * seed + increment) mod 65536
;     multiplier = 25173, increment = 13849
;
; Inputs: none
; Return: (AX) random value

rand:
    push dx
    mov ax, 25173       ; LCG Multiplier
    mul word [seed] ; DX:AX = LCG multiplier * seed
    add ax, 13849       ; Add LCG increment value
    mov [seed], ax      ; Update seed
    ; AX = (multiplier * seed + increment) mod 65536
    pop dx
    ret

seed dw 11             ; Default initial seed of 11

buffer:  times 10 db 0
output_string db 80 dup (?)

item_name: times 30 db 0
item_amount dd 0
pos_x db 0
pos_y db 0
pos_xx db 0

city db 1
cash dd 2000
bank dd 0
debt dd 5500

price_factor1 dw 1
price_factor2 dw 1
price_factor dw 1

chosen_drug  db 0
chosen_units dw 0
units dw 0

day db 1
day_inc db 0

drug_price_base dw 22000, 800, 9000, 10, 2000, 700, 600, 800, 200, 300, 1000, 1000
drug_price dw 22000, 800, 9000, 10, 2000, 700, 600, 800, 200, 300, 1000, 1000
drugs_store dw 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0

enum CITIES, BRONX, GHETTO, CENTRAL_PARK, CONEY_ISLAND, MANHATTAN, BROOKLYN
enum DRUGS, COCAINE, HASHISH, HEROIN, LUDES, MDA, OPIUM, PEYOTE, SHROOMS, SPEED, WEED, ACID, PCP

msg_top db 201, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                205, 205, 205, 205, 205, 205, 205, 205, 205, 187, 0

msg_line db 186, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, \
                 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, \
                 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, \
                 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, \
                 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, \
                 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, \
                 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, \
                 32, 32, 32, 32, 32, 32, 32, 32, 186, 0

msg_line_sep db 204, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 185, 0

msg_screen_top  db 204, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 203, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 203, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 185, 0


msg_screen_middle  db 186, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, \
                 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, \
                 32, 32, 186, 32, 32, 32, 32, 32, 32, 32, \
                 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, \
                 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, \
                 32, 32, 32, 32, 186, 32, 32, 32, 32, 32, \
                 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, \
                 32, 32, 32, 32, 32, 32, 32, 32, 186, 0

msg_screen_bottom  db 204, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 202, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 203, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 202, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 185, 0

msg_screen_middle_line  db 186, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, \
                 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, \
                 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, \
                 32, 32, 32, 32, 32, 32, 32, 32, 186, 32, \
                 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, \
                 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, \
                 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, \
                 32, 32, 32, 32, 32, 32, 32, 32, 186, 0

msg_screen_top_menu db 204, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 202, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                 205, 205, 205, 205, 205, 205, 205, 205, 185, 0

msg_bottom db 200, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
                205, 205, 205, 205, 205, 205, 205, 205, 205, 188, 0

msg_title  db 'Dope Wars', 0

msg_intro_first db 'This is DopeWars.', 0
msg_intro_second db 'Based on John E. Dell', 27h, 's game called Drugwars and its later evolution', 0
msg_intro_third db 'called DopeWars', 0
msg_intro_fourth db 'Buy & Sell drugs, travelling from city to city', 20h, 0
msg_intro_fifth db 'I voluntarily removed guns and killing,', 0 
msg_intro_sixth db 'selling drugs was already enough', 0
msg_intro_seventh db 'Ka 2021 - Press any key to start playing', 20h, 0

intro_ptrs dw msg_intro_first, msg_intro_second, msg_intro_third, msg_intro_fourth, msg_intro_fifth, msg_intro_sixth, msg_intro_seventh

msg_bronx db 'Bronx', 0
msg_ghetto db 'Ghetto', 0
msg_central_park db 'Central Park', 0
msg_coney_island db 'Coney Island', 0
msg_manhattan db 'Manhattan', 0
msg_brooklyn db 'Brooklyn', 0

city_ptrs dw msg_bronx, msg_ghetto, msg_central_park, msg_coney_island, msg_manhattan, msg_brooklyn

msg_stats db 'Stats', 0
msg_trenchcoat db 'Trenchcoat', 0
msg_cash db 'Cash:', 0
msg_guns db 'Guns:', 0
msg_health db 'Health: ', 0
msg_bank db 'Bank:', 0
msg_debt db 'Debt:', 0

msg_cocaine db 'Cocaine', 0
msg_hashish db 'Hashish', 0
msg_heroin db 'Heroin', 0
msg_ludes db 'Ludes', 0
msg_mda db 'MDA', 0
msg_opium db 'Opium', 0
msg_peyote db 'Peyote', 0
msg_shrooms db 'Shrooms', 0
msg_speed db 'Speed', 0
msg_weed db 'Weed', 0
msg_acid db 'Acid', 0
msg_pcp db 'PCP', 0

msg_hey_dude db 'Hey You, the prices of drugs here are...', 0
msg_what_to_do db 'Will you (b)uy, (s)ell or (j)et?', 20h, 0
msg_what_to_buy db 'What do you want to buy?', 20h, 0
msg_what_to_sell db 'What do you want to sell?', 20h, 0
msg_where_to_jet db 'Where do you want to jet?', 20h,0
msg_how_many_units db 'How many?', 20h, 0

msg_date db '-12-1984', 0
msg_end_game db 'Game Over', 0
msg_you_earned db 'You earned', 20h, 0
msg_bank_or_shark db 'Want to visit the (B)ank or (L)oan Shark?', 20h, 0
msg_bank_deposit_or_witdraw db 'Do you want to (D)eposit or (W)ithdraw?', 20h, 0
msg_bank_withdraw db 'How much do you want to withdraw?', 20h, 0
msg_bank_deposit db 'How much do you want to deposit?', 20h, 0
msg_shark db 'How much do you want to repay?', 20h, 0

msg_how_many_you_can_afford db 'You can buy', 20h, 0
msg_how_many_you_have db 'You can sell', 20h, 0
